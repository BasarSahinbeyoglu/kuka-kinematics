#include "Lifter_Plugin.hh"

using namespace gazebo;
// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(Lifter_Plugin)

// Constructor
Lifter_Plugin::Lifter_Plugin() : ModelPlugin()
{
}

// Destructor
Lifter_Plugin::~Lifter_Plugin()
{
}

void Lifter_Plugin::Load(physics::ModelPtr model, sdf::ElementPtr _sdf)
{
  this->model = model;
  this->world = this->model->GetWorld();
  this->world->ResetEntities(gazebo::physics::Base::MODEL);
  this->world->Reset();

  this->robot_namespace = this->model->GetName();

  //Get wheel joints
  if (!_sdf->HasElement("joint_name"))
  {
    this->joint_name = "joint_name";
  } else {
    this->joint_name = _sdf->Get<std::string>("joint_name");
  }

  if (!_sdf->HasElement("torque"))
  {
    this->torque = 5;
  } else {
    this->torque = _sdf->Get<double>("torque");
  }

  if (!_sdf->HasElement("updateRate"))
  {
    this->update_rate = 100.0;
  } else {
    this->update_rate = _sdf->Get<double>("updateRate");
  }

  // Initialize update rate stuff
  if (this->update_rate > 0.0) {
    this->update_period = 1.0 / this->update_rate;
  } else {
    this->update_period = 0.0;
  }

  this->last_update_time = this->world->SimTime().Double();

  // Initialize velocity stuff
  this->lifter_speed = 0;
  this->lifter_joint = this->model->GetJoint(this->joint_name);

  if (!this->lifter_joint) {
    char error[200];
    snprintf(error, 200,
        "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get left front hinge joint named \"%s\"",
        this->robot_namespace.c_str(), this->joint_name.c_str());
    gzthrow(error);
  }

  this->lifter_joint->SetParam("fmax", 0, torque);

  // Initialize ros, if it has not already bee initialized.
  if (!ros::isInitialized())
  {
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, this->robot_namespace,
      ros::init_options::NoSigintHandler);
  }

  // Create our ROS node. This acts in a similar manner to
  // the Gazebo node
  this->rosNode.reset(new ros::NodeHandle(this->robot_namespace));

  this->command_topic = "/" + this->robot_namespace + "/" + this->joint_name + "/set_speed";

  ros::SubscribeOptions so =
    ros::SubscribeOptions::create<std_msgs::Float64>(
      this->command_topic,
      1,
      std::bind(&Lifter_Plugin::SpeedCallback, this, std::placeholders::_1),
      ros::VoidConstPtr(),
      NULL);

  this->cmd_vel_subscriber = this->rosNode->subscribe(so);

  this->worldConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&Lifter_Plugin::Update, this));
}

void Lifter_Plugin::SpeedCallback(const std_msgs::Float64::ConstPtr& msg)
{
  this->lifter_speed = msg->data;
  //std::cout << msg->data << '\n';
}

void Lifter_Plugin::Update()
{
  double current_time = this->world->SimTime().Double();
  double seconds_since_last_update = current_time - this->last_update_time;

  if (seconds_since_last_update > this->update_period)
  {
    this->lifter_joint->SetParam("vel", 0, this->lifter_speed);

    this->last_update_time = this->world->SimTime().Double();
  }
}
